package boiteMail;

import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.runners.Parameterized;

import boiteMail.Courriel;

public class CourrielTest {
	
	Courriel courriel;
	String adresseExiste="blabla";
	String adresseNExiste=null;
	String adresseOk="juleslebg@umontpellier.fr";
	String adresseNOk="marchepas";
	String titreOk="Titre de bg";
	String titreNOk=null;
	String messageOk="blablabla joint blablabla";
	String messageNOk="blablabla";
	ArrayList <String> pieceOk;
	ArrayList <String> pieceNOk;
	String pj="PJ.pdf";
	
	@BeforeEach
	public void setUp(){
		courriel = new Courriel();
		pieceOk = new ArrayList<String>();
		pieceNOk = new ArrayList<String>();
		pieceOk.add(pj);
	}
	
	@Test
	public void testPresenceAdresse() {
		assertEquals(true,courriel.VerifPresenceAdresse(adresseExiste));
		assertEquals(false,courriel.VerifPresenceAdresse(adresseNExiste));
	}
	
	@Test
	public void testAdresse() {
		assertEquals(true,courriel.VerifAdresse(adresseOk));
		assertEquals(false,courriel.VerifAdresse(adresseNOk));
	}
	
	@Test
	public void testTitre() {
		assertEquals(true,courriel.VerifTitre(titreOk));
		assertEquals(false,courriel.VerifTitre(titreNOk));
	}
	
	@Test
	public void testMessage() {
		assertEquals(true,courriel.VerifMessage(messageNOk,pieceOk));
		assertEquals(false,courriel.VerifMessage(messageOk,pieceNOk));
	}
	
	@DisplayName("validerAdresse")
		@ParameterizedTest
		@ValueSource(strings= {"juleslebg@umontpellier.fr","thibaultlepasbou@montpellier.fr","cavaaa@oskour.fr","oskour@hg.fr"})
		void testAdresse2 (String adresse) {
			assertEquals (true,courriel.VerifAdresse(adresse)) ;
		}
	
	@Parameterized.Parameters
    public static Stream<Arguments> AdressesIncorrectes() {
        return Stream.of(
            Arguments.of("@disroot.org", "Rien avant l'arobase"),
            Arguments.of("hola.org", "Pas d'arobase"),
            Arguments.of("hola@.org", "Pas de nom de domaine"),
            Arguments.of("hola@disrootorg", "Pas de point entre nom de domaine et extension de domaine"),
            Arguments.of("hola@disroot.", "Pas de nom d'extension de domaine"),
            Arguments.of("marchepas", "rien ne va")
        );
    }
   
    @ParameterizedTest(name = "test adresse : {0}, problem : {1}")
    @MethodSource("AdressesIncorrectes")
    public void AdressesIncorrectes(String adresse, String probleme) {
    	assertEquals (false,courriel.VerifAdresse(adresse)) ;
    }
    
   


}
