package agl.tp1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestGestionTER {

    public GestionTER TER1;
	
	@BeforeEach
	public void setUp() {
		TER1 = new GestionTER();
	}
	
	@Test
	void testAddGroupe() { //Et teste getGroupe() (donc getNbGroupe() aussi).
		TER1.addGroupe("Groupe0");
		assertEquals("Groupe0",TER1.getGroupe(0).getNom()); //Teste que Groupe0 existe à partir de son nom.
		assertEquals("n'existe pas",TER1.getGroupe(1000).getNom()); //Teste quand le groupe n'existe pas (> getNbGroupe()).
		assertEquals("n'existe pas",TER1.getGroupe(-1).getNom()); //Teste quand le groupe n'existe pas (< 0).
	}
	
	@Test
	void testAddSujet() { //Et teste getSujets().
		TER1.addSujet("Sujet0");
		assertEquals("Sujet0",TER1.getSujets().get(0).getTitre()); //Teste que Sujet0 existe dans l'ArrayList sujets grâce à getSujets().
	}
	
	@Test
	void testToString() {
		TER1.addGroupe("Groupe0"); //Rajouter car il faut le refaire pour chaque @Test
		TER1.addSujet("Sujet0"); //Rajouter car il faut le refaire pour chaque @Test
		assertEquals("Liste des groupes:\n----------------\n"+TER1.getGroupe(0).toString()+"\n"+"\n\nListe des sujets:\n------------------\n"+TER1.getSujets().get(0).toString()+"\n",TER1.toString());
	}
}
