package boiteMail;

import java.util.ArrayList;

public class Courriel {
	
	private String adresse;
	private String titre;
	private String message;
	private ArrayList<String> cheminPJ;
	
	public Courriel() {
		this.adresse = null;
		this.titre = null;
		this.message = null;
		this.cheminPJ = null;
	}
	
	public boolean VerifPresenceAdresse(String adresse) {
		if(adresse != null) {
			return true;
		}else {
			System.out.println("Attention, il n'y a pas de destinataire !");
			return false;
		}
	}
	
	public boolean VerifAdresse(String adresse) {
		if(adresse.matches("(.+)@(.+)\\.(.+)")){
			this.adresse=adresse;
			return true;
		}else {
			System.out.println("Attention, l'adresse mail n'est pas valide !");
			return false;
		}
	}
	
	public boolean VerifTitre(String titre) {
		if(titre!=null) {
			this.titre=titre;
			return true;
		}else {
			System.out.println("Attention, le mail ne possède pas de titre !");
			return false;
		}
	}
	
	public boolean VerifMessage(String message, ArrayList<String> cheminPJ) {
		if(((message.contains("PJ")) || (message.contains("joint")) || (message.contains("jointe"))) && (cheminPJ.isEmpty())){
			System.out.println("Attention, le mail ne contient pas de pièce-jointe !");
			return false;
		}else {
			this.message=message;
			this.cheminPJ=cheminPJ;
			return true;
		}
	}
	
	public void Envoyer(String adresse, String titre, String message, ArrayList<String> cheminPJ) {
		if((VerifPresenceAdresse(adresse)) && (VerifAdresse(adresse)) && (VerifTitre(titre)) && (VerifMessage(message, cheminPJ))) {
			System.out.println("Tout est ok, le mail est envoyé");
		}
	}


}
