package poste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.ColisExpress;
import poste.Colis;
import poste.Recommandation;

public class ColisExpressTest {
	
	ColisExpress colis2;
	ColisExpress colis3;
	float affranchissementColisExpress=30f;
	float toleranceVolume=0.0000001f;
	
	
	
	@BeforeEach
	public void setUp() throws ColisExpressInvalide{
		colis2 = new ColisExpress("Jules", "famille Cassan, LV, France", "34400",24,0.02f,Recommandation.deux,"Louna",200,false);
		colis3 = new ColisExpress("Jules", "famille Cassan, LV, France", "34400",24,0.02f,Recommandation.deux,"Louna",200,true);
	}
	
	@Test
	public void testToString() {
		assertEquals("Colis express 34400/famille Cassan, LV, France/2/0.02/200.0/24.0/4",colis2.toString());
	}
	
	@Test
	public void testAffranchissement() {
		assertEquals(30.0f,colis2.tarifAffranchissement(),affranchissementColisExpress);
		assertEquals(33.0f,colis3.tarifAffranchissement(),affranchissementColisExpress);
	}
	
	@Test
	public void testRemboursement() {
		assertEquals(100f,colis2.tarifRemboursement(),toleranceVolume);
	}
	
	@Test
	public void testCreationColisInvalideDepassePoidsMax(){
		assertThrows(ColisExpressInvalide.class,() -> {
			new ColisExpress("Thibault", "famille Caizergues, Le Grau du Roi, France", "30240",31,0.02f,Recommandation.deux,"blobfish",200,false);
		});
	}

}
